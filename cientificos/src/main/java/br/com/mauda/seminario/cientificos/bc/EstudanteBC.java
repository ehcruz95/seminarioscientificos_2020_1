package br.com.mauda.seminario.cientificos.bc;

import br.com.mauda.seminario.cientificos.dao.EstudanteDao;
import br.com.mauda.seminario.cientificos.dto.EstudanteDTO;
import br.com.mauda.seminario.cientificos.model.Estudante;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class EstudanteBC extends PatternCrudBC<Estudante, EstudanteDTO, EstudanteDao> {
}
