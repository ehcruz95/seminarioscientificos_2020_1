package br.com.mauda.seminario.cientificos.bc;

import br.com.mauda.seminario.cientificos.dao.InstituicaoDao;
import br.com.mauda.seminario.cientificos.dto.InstituicaoDTO;
import br.com.mauda.seminario.cientificos.model.Instituicao;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class InstituicaoBC extends PatternCrudBC<Instituicao, InstituicaoDTO, InstituicaoDao> {
}
