package br.com.mauda.seminario.cientificos.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.Future;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;

@Entity
@Table(name = "TB_SEMINARIO")
public class Seminario implements DataValidation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 50)
    private String titulo;

    @NotBlank
    @Size(max = 200)
    private String descricao;

    @Column(name = "MESA_REDONDA")
    @NotNull
    private Boolean mesaRedonda;

    @Temporal(TemporalType.DATE)
    @NotNull
    @Future
    private Date data;

    @Column(name = "QTD_INSCRICOES")
    @NotNull
    @Min(value = 1)
    private Integer qtdInscricoes;

    @ManyToMany
    @JoinTable(
        name = "TB_PROFESSOR_SEMINARIO",
        joinColumns = @JoinColumn(name = "ID_SEMINARIO"),
        inverseJoinColumns = @JoinColumn(name = "ID_PROFESSOR"))
    @NotEmpty
    @Valid
    private List<Professor> professores = new ArrayList<>();

    @ManyToMany
    @JoinTable(
        name = "TB_SEMINARIO_AREA_CIENTIFICA",
        joinColumns = @JoinColumn(name = "ID_SEMINARIO"),
        inverseJoinColumns = @JoinColumn(name = "ID_AREA_CIENTIFICA"))
    @NotEmpty
    @Valid
    private List<AreaCientifica> areasCientificas = new ArrayList<>();

    @OneToMany(mappedBy = "seminario", cascade = CascadeType.ALL)
    @NotEmpty
    @Valid
    private List<Inscricao> inscricoes = new ArrayList<>();

    private Seminario() {
    }

    public Seminario(AreaCientifica areaCientifica, Professor professor, Integer qtdInscricoes) {
        this.areasCientificas.add(areaCientifica);
        professor.adicionarSeminario(this);
        this.professores.add(professor);
        this.qtdInscricoes = qtdInscricoes;
        for (int i = 0; i < qtdInscricoes; i++) {
            this.inscricoes.add(new Inscricao(this));
        }
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return this.titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean getMesaRedonda() {
        return this.mesaRedonda;
    }

    public void setMesaRedonda(Boolean mesaRedonda) {
        this.mesaRedonda = mesaRedonda;
    }

    public List<Professor> getProfessores() {
        return this.professores;
    }

    public Date getData() {
        return this.data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public List<AreaCientifica> getAreasCientificas() {
        return this.areasCientificas;
    }

    public List<Inscricao> getInscricoes() {
        return this.inscricoes;
    }

    public Integer getQtdInscricoes() {
        return this.qtdInscricoes;
    }

    public void setQtdInscricoes(Integer qtdInscricoes) {
        this.qtdInscricoes = qtdInscricoes;
    }

    @Override
    public void validateForDataModification() {
        this.validateProfessores();
        this.validateAreasCientificas();
    }

    private void validateAreasCientificas() {
        if (this.areasCientificas == null || this.areasCientificas.isEmpty()) {
            throw new SeminariosCientificosException("ER0076");
        }

        for (AreaCientifica areaCientifica : this.areasCientificas) {
            if (areaCientifica == null) {
                throw new SeminariosCientificosException("ER0003");
            }
            areaCientifica.validateForDataModification();
        }

    }

    private void validateProfessores() {
        if (this.professores == null || this.professores.isEmpty()) {
            throw new SeminariosCientificosException("ER0075");
        }

        for (Professor professor : this.professores) {
            if (professor == null) {
                throw new SeminariosCientificosException("ER0003");
            }
            professor.validateForDataModification();
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.id == null ? 0 : this.id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        Seminario other = (Seminario) obj;
        if (this.id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!this.id.equals(other.id)) {
            return false;
        }
        return true;
    }

}
