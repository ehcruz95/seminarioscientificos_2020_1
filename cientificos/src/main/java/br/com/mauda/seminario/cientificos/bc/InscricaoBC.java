package br.com.mauda.seminario.cientificos.bc;

import java.util.Date;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import br.com.mauda.seminario.cientificos.dao.InscricaoDao;
import br.com.mauda.seminario.cientificos.dto.AlunoInscritoSeminarioDTO;
import br.com.mauda.seminario.cientificos.dto.InscricaoDTO;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.interceptor.BeanValidationMethod;
import br.com.mauda.seminario.cientificos.model.Estudante;
import br.com.mauda.seminario.cientificos.model.Inscricao;
import br.com.mauda.seminario.cientificos.model.enums.SituacaoInscricaoEnum;

@ApplicationScoped
public class InscricaoBC extends PatternCrudBC<Inscricao, InscricaoDTO, InscricaoDao> {

    @BeanValidationMethod
    public void comprar(@NotNull @Valid Inscricao inscricao,
        @NotNull @Valid Estudante estudante,
        @NotNull @Valid Boolean direitoMaterial) {
        if (!SituacaoInscricaoEnum.DISPONIVEL.equals(inscricao.getSituacao())) {
            throw new SeminariosCientificosException("ER0042");
        }
        if (new Date().after(inscricao.getSeminario().getData())) {
            throw new SeminariosCientificosException("ER0043");
        }
        inscricao.comprar(estudante, direitoMaterial);
        this.update(inscricao);
    }

    @BeanValidationMethod
    public void cancelarCompra(@NotNull @Valid Inscricao inscricao) {
        if (!SituacaoInscricaoEnum.COMPRADO.equals(inscricao.getSituacao())) {
            throw new SeminariosCientificosException("ER0044");
        }
        if (new Date().after(inscricao.getSeminario().getData())) {
            throw new SeminariosCientificosException("ER0045");
        }
        inscricao.cancelarCompra();
        this.update(inscricao);
    }

    @BeanValidationMethod
    public void realizarCheckIn(@NotNull @Valid Inscricao inscricao) {
        if (!SituacaoInscricaoEnum.COMPRADO.equals(inscricao.getSituacao())) {
            throw new SeminariosCientificosException("ER0046");
        }
        if (new Date().after(inscricao.getSeminario().getData())) {
            throw new SeminariosCientificosException("ER0047");
        }
        inscricao.realizarCheckIn();
        this.update(inscricao);
    }

    @BeanValidationMethod
    public List<AlunoInscritoSeminarioDTO> getAlunosInscritosSeminario(@NotBlank String tituloSeminario) {
        return this.dao.getAlunosInscritosSeminario(tituloSeminario);
    }

}
