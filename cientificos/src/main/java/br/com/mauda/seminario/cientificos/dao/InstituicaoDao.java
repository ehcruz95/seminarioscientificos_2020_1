package br.com.mauda.seminario.cientificos.dao;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Query;
import org.hibernate.Session;

import br.com.mauda.seminario.cientificos.dto.InstituicaoDTO;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.interceptor.Cronometro;
import br.com.mauda.seminario.cientificos.model.Instituicao;

@ApplicationScoped
public class InstituicaoDao extends PatternCrudDAO<Instituicao, InstituicaoDTO> implements HQLClause {

    public InstituicaoDao() {
        super(Instituicao.class);
    }

    @Override
    public void inicializaLazyObjects(Instituicao object) {
        // nenhum objeto a ser inicializado
    }

    @Override
    @Cronometro
    public Collection<Instituicao> findByFilter(InstituicaoDTO filter) {
        Session session = this.getSession();

        try {
            StringBuilder sb = new StringBuilder("FROM Instituicao i");
            boolean whereFlag = false;
            Map<String, Object> parameters = new HashMap<>();

            if (!StringUtils.isBlank(filter.getCidade())) {
                sb.append(this.validateHqlClause(whereFlag)).append("i.cidade LIKE :cidade");
                parameters.put("cidade", "%" + filter.getCidade() + "%");
            }
            if (!StringUtils.isBlank(filter.getEstado())) {
                sb.append(this.validateHqlClause(whereFlag)).append("i.estado LIKE :estado");
                parameters.put("estado", "%" + filter.getEstado() + "%");
            }
            if (filter.getId() != null) {
                sb.append(this.validateHqlClause(whereFlag)).append("i.id = :id");
                parameters.put("id", filter.getId());
            }
            if (!StringUtils.isBlank(filter.getNome())) {
                sb.append(this.validateHqlClause(whereFlag)).append("i.nome LIKE :nome");
                parameters.put("nome", "%" + filter.getNome() + "%");
            }
            if (!StringUtils.isBlank(filter.getPais())) {
                sb.append(this.validateHqlClause(whereFlag)).append("i.pais LIKE :pais");
                parameters.put("pais", "%" + filter.getPais() + "%");
            }
            if (!StringUtils.isBlank(filter.getSigla())) {
                sb.append(this.validateHqlClause(whereFlag)).append("i.sigla LIKE :sigla");
                parameters.put("sigla", "%" + filter.getSigla() + "%");
            }

            Query q = session.createQuery(sb.toString());

            parameters.forEach(q::setParameter);

            return q.list();
        } catch (Exception e) {
            throw new SeminariosCientificosException(e);
        }
    }
}