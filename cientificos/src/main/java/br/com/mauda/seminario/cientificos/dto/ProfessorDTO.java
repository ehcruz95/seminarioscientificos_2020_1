package br.com.mauda.seminario.cientificos.dto;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

public class ProfessorDTO implements Serializable, FilterValidation {

    /**
     *
     */
    private static final long serialVersionUID = 8648189528268965894L;
    private Long id;
    private String cidade;
    private String email;
    private String estado;
    private String nome;
    private String nomeInstituicao;
    private String pais;
    private Double salario;
    private String telefone;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCidade() {
        return this.cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNomeInstituicao() {
        return this.nomeInstituicao;
    }

    public void setNomeInstituicao(String nomeInstituicao) {
        this.nomeInstituicao = nomeInstituicao;
    }

    public String getPais() {
        return this.pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public Double getSalario() {
        return this.salario;
    }

    public void setSalario(Double salario) {
        this.salario = salario;
    }

    public String getTelefone() {
        return this.telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    @Override
    public boolean validateForFindData() {
        return this.isInstituicaoFieldsFilled() || this.isProfessorFieldsFilled();
    }

    private boolean isInstituicaoFieldsFilled() {
        return !StringUtils.isBlank(this.cidade) || !StringUtils.isBlank(this.estado)
            || !StringUtils.isBlank(this.nomeInstituicao) || !StringUtils.isBlank(this.pais);
    }

    private boolean isProfessorFieldsFilled() {
        return this.id != null || this.salario != null || !StringUtils.isBlank(this.email)
            || !StringUtils.isBlank(this.nome) || !StringUtils.isBlank(this.telefone);
    }
}
