package br.com.mauda.seminario.cientificos.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import br.com.mauda.seminario.cientificos.model.enums.SituacaoInscricaoEnum;

@Entity
@Table(name = "TB_INSCRICAO")
public class Inscricao implements DataValidation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "DIREITO_MATERIAL")
    private Boolean direitoMaterial;

    @ManyToOne
    @JoinColumn(name = "ID_SEMINARIO")
    @NotNull
    @Valid
    private Seminario seminario;

    @ManyToOne
    @JoinColumn(name = "ID_ESTUDANTE")
    private Estudante estudante;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "smallint")
    @NotNull
    private SituacaoInscricaoEnum situacao = SituacaoInscricaoEnum.DISPONIVEL;

    private Inscricao() {
    }

    public Inscricao(Seminario seminario) {
        this.seminario = seminario;
    }

    public Seminario getSeminario() {
        return this.seminario;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getDireitoMaterial() {
        return this.direitoMaterial;
    }

    public SituacaoInscricaoEnum getSituacao() {
        return this.situacao;
    }

    public Estudante getEstudante() {
        return this.estudante;
    }

    public void cancelarCompra() {
        this.situacao = SituacaoInscricaoEnum.DISPONIVEL;
        this.estudante.removerInscricao(this);
        this.direitoMaterial = null;
        this.estudante = null;
    }

    public void comprar(Estudante estudante, Boolean direitoMaterial) {
        this.situacao = SituacaoInscricaoEnum.COMPRADO;
        this.direitoMaterial = direitoMaterial;
        this.estudante = estudante;
        this.estudante.adicionarInscricao(this);
    }

    public void realizarCheckIn() {
        this.situacao = SituacaoInscricaoEnum.CHECKIN;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.id == null ? 0 : this.id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        Inscricao other = (Inscricao) obj;
        if (this.id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!this.id.equals(other.id)) {
            return false;
        }
        return true;
    }

}
