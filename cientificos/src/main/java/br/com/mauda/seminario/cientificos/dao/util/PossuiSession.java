package br.com.mauda.seminario.cientificos.dao.util;

import org.hibernate.Session;

public interface PossuiSession {

    Session getSession();

}
