package br.com.mauda.seminario.cientificos.interceptor;

import java.util.Set;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;

@Interceptor
@BeanValidationMethod
public class BeanValidationMethodInterceptor {

    @AroundInvoke
    public Object manage(InvocationContext ctx) throws Exception {
        Set<ConstraintViolation<Object>> violations = Validation.buildDefaultValidatorFactory()
            .getValidator()
            .forExecutables()
            .validateParameters(ctx.getTarget(), ctx.getMethod(), ctx.getParameters());
        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
        return ctx.proceed();
    }
}
