package br.com.mauda.seminario.cientificos.dto;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

public class InstituicaoDTO implements Serializable, FilterValidation {

    /**
     *
     */
    private static final long serialVersionUID = 1217168242891924853L;
    private Long id;
    private String cidade;
    private String estado;
    private String nome;
    private String pais;
    private String sigla;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCidade() {
        return this.cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getPais() {
        return this.pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getSigla() {
        return this.sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    @Override
    public boolean validateForFindData() {
        return !StringUtils.isBlank(this.cidade) || !StringUtils.isBlank(this.estado)
            || !StringUtils.isBlank(this.nome) || !StringUtils.isBlank(this.pais)
            || !StringUtils.isBlank(this.sigla) || this.id != null;
    }

}
