package br.com.mauda.seminario.cientificos.dao;

import java.util.Collection;

import javax.enterprise.context.ApplicationScoped;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import br.com.mauda.seminario.cientificos.dto.CursoDTO;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.interceptor.Cronometro;
import br.com.mauda.seminario.cientificos.model.Curso;

@ApplicationScoped
public class CursoDao extends PatternCrudDAO<Curso, CursoDTO> {

    public CursoDao() {
        super(Curso.class);
    }

    @Override
    public void inicializaLazyObjects(Curso object) {
        if (object != null) {
            Hibernate.initialize(object.getAreaCientifica().getCursos());
        }
    }

    @Override
    @Cronometro
    public Collection<Curso> findByFilter(CursoDTO filter) {
        Session session = this.getSession();

        try {
            Criteria c = session.createCriteria(Curso.class, "curso");
            c.createAlias("curso.areaCientifica", "areaCientifica");

            if (filter.getIdAreaCientifica() != null) {
                c.add(Restrictions.eq("areaCientifica.id", filter.getIdAreaCientifica()));
            }

            if (filter.getId() != null) {
                c.add(Restrictions.eq("curso.id", filter.getId()));
            }

            if (!StringUtils.isBlank(filter.getNomeAreaCientifica())) {
                c.add(Restrictions.like("areaCientifica.nome", "%" + filter.getNomeAreaCientifica() + "%"));
            }

            if (!StringUtils.isBlank(filter.getNome())) {
                c.add(Restrictions.like("curso.nome", "%" + filter.getNome() + "%"));
            }

            Collection<Curso> cursos = c.list();
            cursos.forEach(this::inicializaLazyObjects);

            return cursos;
        } catch (Exception e) {
            throw new SeminariosCientificosException(e);
        }
    }
}