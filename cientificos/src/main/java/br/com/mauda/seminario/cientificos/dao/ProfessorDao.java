package br.com.mauda.seminario.cientificos.dao;

import java.util.Collection;

import javax.enterprise.context.ApplicationScoped;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import br.com.mauda.seminario.cientificos.dto.ProfessorDTO;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.interceptor.Cronometro;
import br.com.mauda.seminario.cientificos.model.Professor;

@ApplicationScoped
public class ProfessorDao extends PatternCrudDAO<Professor, ProfessorDTO> {

    public ProfessorDao() {
        super(Professor.class);
    }

    @Override
    public void inicializaLazyObjects(Professor object) {
        if (object != null) {
            object.getSeminarios().forEach(Hibernate::initialize);
        }
    }

    @Override
    @Cronometro
    public Collection<Professor> findByFilter(ProfessorDTO filter) {
        Session session = this.getSession();

        try {
            Criteria c = session.createCriteria(Professor.class, "p");
            c.createAlias("p.instituicao", "i");

            if (!StringUtils.isBlank(filter.getCidade())) {
                c.add(Restrictions.ilike("i.cidade", "%" + filter.getCidade() + "%"));
            }

            if (!StringUtils.isBlank(filter.getEstado())) {
                c.add(Restrictions.ilike("i.estado", "%" + filter.getEstado() + "%"));
            }

            if (!StringUtils.isBlank(filter.getNomeInstituicao())) {
                c.add(Restrictions.ilike("i.nome", "%" + filter.getNomeInstituicao() + "%"));
            }

            if (!StringUtils.isBlank(filter.getPais())) {
                c.add(Restrictions.ilike("i.pais", "%" + filter.getPais() + "%"));
            }

            if (!StringUtils.isBlank(filter.getEmail())) {
                c.add(Restrictions.ilike("p.email", "%" + filter.getEmail() + "%"));
            }

            if (filter.getId() != null && filter.getId() >= 0) {
                c.add(Restrictions.eq("p.id", filter.getId()));
            }

            if (!StringUtils.isBlank(filter.getNome())) {
                c.add(Restrictions.ilike("p.nome", "%" + filter.getNome() + "%"));
            }

            if (filter.getSalario() != null && filter.getSalario() >= 0) {
                c.add(Restrictions.eq("p.salario", filter.getSalario()));
            }

            if (!StringUtils.isBlank(filter.getTelefone())) {
                c.add(Restrictions.ilike("p.telefone", "%" + filter.getTelefone() + "%"));
            }

            Collection<Professor> professores = c.list();
            professores.forEach(this::inicializaLazyObjects);

            return professores;
        } catch (Exception e) {
            throw new SeminariosCientificosException(e);
        }
    }
}