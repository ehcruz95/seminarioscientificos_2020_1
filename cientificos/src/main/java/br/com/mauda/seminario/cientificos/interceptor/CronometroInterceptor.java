package br.com.mauda.seminario.cientificos.interceptor;

import java.util.concurrent.TimeUnit;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import org.apache.commons.lang3.time.StopWatch;
import org.apache.logging.log4j.LogManager;

@Interceptor
@Cronometro
public class CronometroInterceptor {

    @AroundInvoke
    public Object manage(InvocationContext ctx) throws Exception {
        StopWatch cronometro = StopWatch.createStarted();
        try {
            return ctx.proceed();
        } finally {
            if (cronometro.isStarted()) {
                cronometro.stop();
            }
            LogManager.getLogger(ctx.getTarget().getClass())
                .debug("{} : executado em {} ms", ctx.getMethod(), cronometro.getTime(TimeUnit.MILLISECONDS));
        }
    }
}
