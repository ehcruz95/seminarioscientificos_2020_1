package br.com.mauda.seminario.cientificos.bc;

import br.com.mauda.seminario.cientificos.dao.CursoDao;
import br.com.mauda.seminario.cientificos.dto.CursoDTO;
import br.com.mauda.seminario.cientificos.model.Curso;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class CursoBC extends PatternCrudBC<Curso, CursoDTO, CursoDao> {
}
