package br.com.mauda.seminario.cientificos.bc;

import br.com.mauda.seminario.cientificos.dao.AreaCientificaDao;
import br.com.mauda.seminario.cientificos.dto.AreaCientificaDTO;
import br.com.mauda.seminario.cientificos.model.AreaCientifica;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class AreaCientificaBC extends PatternCrudBC<AreaCientifica, AreaCientificaDTO, AreaCientificaDao> {
}
