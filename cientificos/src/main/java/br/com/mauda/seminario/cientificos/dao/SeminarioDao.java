package br.com.mauda.seminario.cientificos.dao;

import java.util.Collection;

import javax.enterprise.context.ApplicationScoped;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.mauda.seminario.cientificos.dto.SeminarioDTO;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.interceptor.Cronometro;
import br.com.mauda.seminario.cientificos.model.Seminario;

@ApplicationScoped
public class SeminarioDao extends PatternCrudDAO<Seminario, SeminarioDTO> {

    public SeminarioDao() {
        super(Seminario.class);
    }

    @Override
    public void inicializaLazyObjects(Seminario object) {
        if (object != null) {
            Hibernate.initialize(object.getAreasCientificas());
            object.getProfessores().forEach(professor -> Hibernate.initialize(professor.getSeminarios()));
            Hibernate.initialize(object.getInscricoes());
        }
    }

    @Override
    @Cronometro
    public Collection<Seminario> findByFilter(SeminarioDTO filter) {
        Session session = this.getSession();

        try {

            Criteria c = session.createCriteria(Seminario.class, "s");

            if (filter.getData() != null) {
                c.add(Restrictions.eq("s.data", filter.getData()));
            }

            if (StringUtils.isNotBlank(filter.getDescricao())) {
                c.add(Restrictions.like("s.descricao", filter.getDescricao(), MatchMode.ANYWHERE));
            }

            if (filter.getId() != null) {
                c.add(Restrictions.eq("s.id", filter.getId()));
            }

            if (filter.getMesaRedonda() != null) {
                c.add(Restrictions.eq("s.mesaRedonda", filter.getMesaRedonda()));
            }

            if (StringUtils.isNotBlank(filter.getNomeAreaCientifica())) {
                c.createAlias("s.areasCientificas", "a");
                c.add(Restrictions.like("a.nome", filter.getNomeAreaCientifica(), MatchMode.ANYWHERE));
            }

            if (StringUtils.isNotBlank(filter.getNomeProfessor())) {
                c.createAlias("s.professores", "p");
                c.add(Restrictions.like("p.nome", filter.getNomeProfessor(), MatchMode.ANYWHERE));
            }

            if (StringUtils.isNotBlank(filter.getTitulo())) {
                c.add(Restrictions.like("s.titulo", filter.getTitulo(), MatchMode.ANYWHERE));
            }

            Collection<Seminario> seminarios = c.list();
            seminarios.forEach(this::inicializaLazyObjects);
            return seminarios;

        } catch (Exception e) {
            throw new SeminariosCientificosException(e.getCause());
        }
    }
}