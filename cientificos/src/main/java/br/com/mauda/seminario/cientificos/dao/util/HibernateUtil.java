package br.com.mauda.seminario.cientificos.dao.util;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

/**
 * @author Mauda
 *
 */
public class HibernateUtil {

    private HibernateUtil() {
    }

    @Produces
    @ApplicationScoped
    public SessionFactory getSessionFactory() {
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry = new ServiceRegistryBuilder()
            .applySettings(configuration.getProperties()).buildServiceRegistry();
        return configuration.buildSessionFactory(serviceRegistry);
    }

    @Produces
    public Session getSession(SessionFactory factory) {
        return factory.openSession();
    }

    public void close(@Disposes Session session) {
        session.close();
    }

    public void close(@Disposes SessionFactory session) {
        session.close();
    }
}
