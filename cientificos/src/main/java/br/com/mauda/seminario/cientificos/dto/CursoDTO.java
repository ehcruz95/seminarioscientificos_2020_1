package br.com.mauda.seminario.cientificos.dto;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

public class CursoDTO implements Serializable, FilterValidation {

    /**
     *
     */
    private static final long serialVersionUID = 1242267600210237927L;
    private Long id;
    private Long idAreaCientifica;
    private String nome;
    private String nomeAreaCientifica;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdAreaCientifica() {
        return this.idAreaCientifica;
    }

    public void setIdAreaCientifica(Long idAreaCientifica) {
        this.idAreaCientifica = idAreaCientifica;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNomeAreaCientifica() {
        return this.nomeAreaCientifica;
    }

    public void setNomeAreaCientifica(String nomeAreaCientifica) {
        this.nomeAreaCientifica = nomeAreaCientifica;
    }

    @Override
    public boolean validateForFindData() {
        return this.isIdFilled() || this.isNomeFilled();
    }

    private boolean isIdFilled() {
        return this.idAreaCientifica != null || this.id != null;
    }

    private boolean isNomeFilled() {
        return !StringUtils.isBlank(this.nome) || !StringUtils.isBlank(this.nomeAreaCientifica);
    }

}
