package br.com.mauda.seminario.cientificos.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import br.com.mauda.seminario.cientificos.util.EmailUtils;

@Entity
@Table(name = "TB_ESTUDANTE")
public class Estudante implements DataValidation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 50)
    private String nome;

    @NotBlank
    @Size(max = 15)
    private String telefone;

    @NotBlank
    @Email(regexp = EmailUtils.EMAIL_PATTERN)
    @Size(max = 50)
    private String email;

    @ManyToOne
    @JoinColumn(name = "ID_INSTITUICAO")
    @NotNull
    @Valid
    private Instituicao instituicao;

    @OneToMany(mappedBy = "estudante")
    private List<Inscricao> inscricoes = new ArrayList<>();

    private Estudante() {
    }

    public Estudante(Instituicao instituicao) {
        this.instituicao = instituicao;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return this.telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Instituicao getInstituicao() {
        return this.instituicao;
    }

    public List<Inscricao> getInscricoes() {
        return this.inscricoes;
    }

    public void adicionarInscricao(Inscricao inscricao) {
        if (inscricao != null) {
            this.inscricoes.add(inscricao);
        }
    }

    public boolean possuiInscricao(Inscricao inscricao) {
        if (inscricao != null) {
            return this.inscricoes.contains(inscricao);
        }
        return false;
    }

    public void removerInscricao(Inscricao inscricao) {
        if (inscricao != null) {
            this.inscricoes.remove(inscricao);
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.id == null ? 0 : this.id.hashCode());
        result = prime * result + (this.telefone == null ? 0 : this.telefone.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        Estudante other = (Estudante) obj;
        if (this.id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!this.id.equals(other.id)) {
            return false;
        }
        if (this.telefone == null) {
            if (other.telefone != null) {
                return false;
            }
        } else if (!this.telefone.equals(other.telefone)) {
            return false;
        }
        return true;
    }

}
