package br.com.mauda.seminario.cientificos.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import br.com.mauda.seminario.cientificos.util.EmailUtils;

@Entity
@Table(name = "TB_PROFESSOR")
public class Professor implements DataValidation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 50)
    @Email(regexp = EmailUtils.EMAIL_PATTERN)
    private String email;

    @NotBlank
    @Size(max = 50)
    private String nome;

    @NotNull
    @DecimalMin(value = "0.01")
    private Double salario;

    @NotBlank
    @Size(max = 15)
    private String telefone;

    @ManyToMany(mappedBy = "professores")
    private List<Seminario> seminarios = new ArrayList<>();

    @OneToOne
    @JoinColumn(name = "ID_INSTITUICAO")
    @NotNull
    @Valid
    private Instituicao instituicao;

    private Professor() {
    }

    public Professor(Instituicao instituicao) {
        this.instituicao = instituicao;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getSalario() {
        return this.salario;
    }

    public void setSalario(Double salario) {
        this.salario = salario;
    }

    public String getTelefone() {
        return this.telefone;
    }

    public Instituicao getInstituicao() {
        return this.instituicao;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public List<Seminario> getSeminarios() {
        return this.seminarios;
    }

    public void adicionarSeminario(Seminario seminario) {
        if (seminario != null) {
            this.seminarios.add(seminario);
        }
    }

    public boolean possuiSeminario(Seminario seminario) {
        if (seminario != null) {
            return this.seminarios.contains(seminario);
        }
        return false;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.id == null ? 0 : this.id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        Professor other = (Professor) obj;
        if (this.id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!this.id.equals(other.id)) {
            return false;
        }
        return true;
    }

}
