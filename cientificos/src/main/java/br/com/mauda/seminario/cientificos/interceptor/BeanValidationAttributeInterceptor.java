package br.com.mauda.seminario.cientificos.interceptor;

import org.apache.logging.log4j.LogManager;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.validation.*;
import java.util.HashSet;
import java.util.Set;

@Interceptor
@BeanValidationAttribute
public class BeanValidationAttributeInterceptor {

    @AroundInvoke
    public Object manage(InvocationContext ctx) throws Exception {
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        Set<ConstraintViolation<?>> violations = new HashSet<>();
        for (Object params : ctx.getParameters()) {
            if (params != null) {
                try {
                    Validator validator = validatorFactory.getValidator();
                    violations.addAll(validator.validate(params));
                } catch (UnexpectedTypeException e) {
                    LogManager.getLogger(ctx.getTarget().getClass())
                            .error("{} - Erro ao validar com o Bean Validations {}", ctx.getMethod(), e.getMessage());
                }
            }
        }
        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
        return ctx.proceed();
    }
}
