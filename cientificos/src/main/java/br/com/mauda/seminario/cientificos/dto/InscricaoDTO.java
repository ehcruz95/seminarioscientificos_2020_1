package br.com.mauda.seminario.cientificos.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import br.com.mauda.seminario.cientificos.model.enums.SituacaoInscricaoEnum;

public class InscricaoDTO implements Serializable, FilterValidation {

    /**
     *
     */
    private static final long serialVersionUID = 6068791725752108146L;
    private Long id;
    private Date dataSeminario;
    private Boolean direitoMaterial;
    private String nomeEstudante;
    private List<SituacaoInscricaoEnum> situacoes = new ArrayList<>();
    private String tituloSeminario;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDataSeminario() {
        return this.dataSeminario;
    }

    public void setDataSeminario(Date dataSeminario) {
        this.dataSeminario = dataSeminario;
    }

    public Boolean getDireitoMaterial() {
        return this.direitoMaterial;
    }

    public void setDireitoMaterial(Boolean direitoMaterial) {
        this.direitoMaterial = direitoMaterial;
    }

    public String getNomeEstudante() {
        return this.nomeEstudante;
    }

    public void setNomeEstudante(String nomeEstudante) {
        this.nomeEstudante = nomeEstudante;
    }

    public List<SituacaoInscricaoEnum> getSituacoes() {
        return this.situacoes;
    }

    public void setSituacoes(List<SituacaoInscricaoEnum> situacoes) {
        this.situacoes = situacoes;
    }

    public String getTituloSeminario() {
        return this.tituloSeminario;
    }

    public void setTituloSeminario(String tituloSeminario) {
        this.tituloSeminario = tituloSeminario;
    }

    @Override
    public boolean validateForFindData() {
        return this.isInscricaoFieldsFilled() || this.isEstudanteFieldsFilled() || this.isSeminarioFieldsFilled();
    }

    private boolean isSeminarioFieldsFilled() {
        return StringUtils.isNotBlank(this.tituloSeminario);
    }

    private boolean isEstudanteFieldsFilled() {
        return StringUtils.isNotBlank(this.nomeEstudante);
    }

    private boolean isInscricaoFieldsFilled() {
        // DATA SEMINARIO OU DATA DA INSCRICAO NO SEMINARIO
        return this.dataSeminario != null || this.direitoMaterial != null
            || this.id != null || !this.situacoes.isEmpty();
    }

}
