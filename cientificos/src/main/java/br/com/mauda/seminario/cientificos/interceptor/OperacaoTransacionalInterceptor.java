package br.com.mauda.seminario.cientificos.interceptor;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.mauda.seminario.cientificos.dao.util.PossuiSession;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;

@Interceptor
@OperacaoTransacional
public class OperacaoTransacionalInterceptor {

    @AroundInvoke
    public Object manage(InvocationContext ctx) throws Exception {
        Session session;
        if (ctx.getTarget() instanceof PossuiSession) {
            session = ((PossuiSession) ctx.getTarget()).getSession();
        } else {
            throw new SeminariosCientificosException("Não é uma instância de PossuiSession");
        }
        Transaction tx = null;
        tx = session.beginTransaction();
        Object result = ctx.proceed();
        try {
            tx.commit();
        } catch (Exception ex) {
            tx.rollback();
            throw new SeminariosCientificosException(ex);
        }
        return result;
    }

}
