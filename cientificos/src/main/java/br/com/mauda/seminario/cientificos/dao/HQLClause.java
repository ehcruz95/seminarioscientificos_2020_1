package br.com.mauda.seminario.cientificos.dao;

/**
 * <h2>HQLClause</h2>
 * <p>
 * Interface usada para adicionar clausulas ao uma query <b>hql</b>
 * <p>
 *
 * @author Eduardo Cruz
 *
 */
public interface HQLClause {

    /**
     * <b>Verificação de clausula sql</b> <br>
     * Método que verfica se a query hql necessita de uma clausula "WHERE" ou "AND" de acorodo com o valor da flag passada;
     *
     * @param flag
     *            - boolean
     * @return String
     */
    default String validateHqlClause(boolean flag) {
        if (!flag) {
            return " WHERE ";
        }
        return " AND ";
    }

}
