package br.com.mauda.seminario.cientificos.bc;

import java.util.Collection;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;

import br.com.mauda.seminario.cientificos.dao.PatternCrudDAO;
import br.com.mauda.seminario.cientificos.dto.FilterValidation;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.interceptor.BeanValidationAttribute;
import br.com.mauda.seminario.cientificos.interceptor.BeanValidationMethod;
import br.com.mauda.seminario.cientificos.interceptor.Cronometro;
import br.com.mauda.seminario.cientificos.model.DataValidation;

public abstract class PatternCrudBC<T extends DataValidation, F extends FilterValidation, D extends PatternCrudDAO<T, F>> {

    @Inject
    protected D dao;

    ///////////////////////////////////////////////////////////////////
    // METODOS DE MODIFICACAO
    ///////////////////////////////////////////////////////////////////

    /**
     * Utilizado para realizar a validacao do object e a chamada do metodo de armazenamento correspondente da classe DAO
     *
     * Devera verificar se o objeto esta de acordo com as regras negociais para ser atualizado na base de dados.
     *
     * @param object
     */
    @BeanValidationMethod
    @BeanValidationAttribute
    public void insert(@NotNull T object) {
        object.validateForDataModification();
        this.dao.insert(object);
    }

    /**
     * Utilizado para realizar a validacao do object e a chamada do metodo de atualizacao correspondente na classe DAO.
     *
     * Devera verificar se o objeto esta de acordo com as regras de negocio para ser atualizado na base de dados.
     *
     * @param object
     * @return
     */
    @BeanValidationMethod
    @BeanValidationAttribute
    public void update(@NotNull T object) {
        object.validateForDataModification();
        this.dao.update(object);
    }

    /**
     * Utilizado para chamar um metodo de delecao correspondente na classe DAO.
     *
     * Devera verificar se o objeto passado nao eh null
     *
     * @param object
     * @return
     */
    @BeanValidationMethod
    @BeanValidationAttribute
    public void delete(@NotNull T object) {
        this.dao.delete(object);
    }

    ///////////////////////////////////////////////////////////////////
    // METODOS DE BUSCA
    ///////////////////////////////////////////////////////////////////

    /**
     * Utilizado para verificar se o id passado nao eh nulo e chamar o metodo de busca correspondente da classe DAO
     *
     * Devera verificar se o id eh negativo ou null
     *
     * @param id
     * @return
     */
    public T findById(Long id) {
        if (id < 0) {
            return null;
        }
        return this.dao.findById(id);
    }

    /**
     * Utilizado para buscas com o filtro da entidade, onde este conterah as informacoes relacionadas com a filtragem de dados
     *
     * @param filter
     * @return
     */
    @Cronometro
    @BeanValidationMethod
    public Collection<T> findByFilter(@NotNull F filter) {
        if (!filter.validateForFindData()) {
            throw new SeminariosCientificosException("ER0001");
        }
        return this.dao.findByFilter(filter);
    }

    /**
     * Utilizado para retornar todas as instancias de uma determinada classe, atraves do metodo de busca correspondente da classe DAO
     *
     * @return
     */
    public Collection<T> findAll() {
        return this.dao.findAll();
    }
}
