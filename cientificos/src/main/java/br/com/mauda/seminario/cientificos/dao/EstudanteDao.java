package br.com.mauda.seminario.cientificos.dao;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;

import br.com.mauda.seminario.cientificos.dto.EstudanteDTO;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.interceptor.Cronometro;
import br.com.mauda.seminario.cientificos.model.Estudante;

@ApplicationScoped
public class EstudanteDao extends PatternCrudDAO<Estudante, EstudanteDTO> implements HQLClause {

    public EstudanteDao() {
        super(Estudante.class);
    }

    @Override
    public void inicializaLazyObjects(Estudante object) {
        if (object != null) {
            Hibernate.initialize(object.getInscricoes());
        }
    }

    @Override
    @Cronometro
    public Collection<Estudante> findByFilter(EstudanteDTO filter) {
        Session session = this.getSession();

        try {
            StringBuilder sb = new StringBuilder("SELECT e FROM Estudante e JOIN e.instituicao i");
            Map<String, Object> parameters = new HashMap<>();
            boolean whereFlag = false;

            if (!StringUtils.isBlank(filter.getCidade())) {
                sb.append(this.validateHqlClause(whereFlag)).append("i.cidade LIKE :cidade");
                parameters.put("cidade", "%" + filter.getCidade() + "%");
            }
            if (!StringUtils.isBlank(filter.getEstado())) {
                sb.append(this.validateHqlClause(whereFlag)).append("i.estado LIKE :estado");
                parameters.put("estado", "%" + filter.getEstado() + "%");
            }
            if (!StringUtils.isBlank(filter.getNomeInstituicao())) {
                sb.append(this.validateHqlClause(whereFlag)).append("i.nome LIKE :nomeInstituicao");
                parameters.put("nomeInstituicao", "%" + filter.getNomeInstituicao() + "%");
            }
            if (!StringUtils.isBlank(filter.getPais())) {
                sb.append(this.validateHqlClause(whereFlag)).append("i.pais LIKE :pais");
                parameters.put("pais", "%" + filter.getPais() + "%");
            }
            if (!StringUtils.isBlank(filter.getEmail())) {
                sb.append(this.validateHqlClause(whereFlag)).append("e.email LIKE :email");
                parameters.put("email", "%" + filter.getEmail() + "%");
            }
            if (filter.getId() != null) {
                sb.append(this.validateHqlClause(whereFlag)).append("e.id = :id");
                parameters.put("id", filter.getId());
            }
            if (!StringUtils.isBlank(filter.getNome())) {
                sb.append(this.validateHqlClause(whereFlag)).append("e.nome LIKE :nomeEstudante");
                parameters.put("nomeEstudante", "%" + filter.getNome() + "%");
            }
            if (!StringUtils.isBlank(filter.getTelefone())) {
                sb.append(this.validateHqlClause(whereFlag)).append("e.telefone LIKE :telefone");
                parameters.put("telefone", "%" + filter.getTelefone() + "%");
            }

            Query q = session.createQuery(sb.toString());

            parameters.forEach(q::setParameter);

            Collection<Estudante> estudantes = q.list();
            estudantes.forEach(this::inicializaLazyObjects);
            return estudantes;

        } catch (Exception e) {
            throw new SeminariosCientificosException(e);
        }
    }
}