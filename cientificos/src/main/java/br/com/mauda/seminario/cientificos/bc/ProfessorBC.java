package br.com.mauda.seminario.cientificos.bc;

import br.com.mauda.seminario.cientificos.dao.ProfessorDao;
import br.com.mauda.seminario.cientificos.dto.ProfessorDTO;
import br.com.mauda.seminario.cientificos.model.Professor;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class ProfessorBC extends PatternCrudBC<Professor, ProfessorDTO, ProfessorDao> {
}
