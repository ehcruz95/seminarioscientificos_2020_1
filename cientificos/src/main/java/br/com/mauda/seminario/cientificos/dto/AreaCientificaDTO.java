package br.com.mauda.seminario.cientificos.dto;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

public class AreaCientificaDTO implements Serializable, FilterValidation {

    /**
     *
     */
    private static final long serialVersionUID = -6446634839457640755L;
    private Long id;
    private String nome;

    @Override
    public boolean validateForFindData() {
        return !StringUtils.isEmpty(this.nome) || this.id != null;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}
