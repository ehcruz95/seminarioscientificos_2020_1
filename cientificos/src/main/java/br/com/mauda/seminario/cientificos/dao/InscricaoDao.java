package br.com.mauda.seminario.cientificos.dao;

import java.util.Collection;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.mauda.seminario.cientificos.dto.AlunoInscritoSeminarioDTO;
import br.com.mauda.seminario.cientificos.dto.InscricaoDTO;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.interceptor.Cronometro;
import br.com.mauda.seminario.cientificos.model.Inscricao;

@ApplicationScoped
public class InscricaoDao extends PatternCrudDAO<Inscricao, InscricaoDTO> {

    private static final long serialVersionUID = 1L;

    public InscricaoDao() {
        super(Inscricao.class);
    }

    @Override
    public void inicializaLazyObjects(Inscricao object) {
        if (object != null) {
            Hibernate.initialize(object.getSeminario().getProfessores());
            object.getSeminario().getProfessores().forEach(p -> Hibernate.initialize(p.getSeminarios()));
            Hibernate.initialize(object.getSeminario().getAreasCientificas());
            object.getSeminario().getInscricoes().forEach(i -> Hibernate.initialize(i.getSeminario()));
            if (object.getEstudante() != null) {
                Hibernate.initialize(object.getEstudante().getInscricoes());
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    @Cronometro
    public Collection<Inscricao> findByFilter(InscricaoDTO filter) {
        Session session = this.getSession();

        try {
            Criteria c = session.createCriteria(Inscricao.class, "i");

            if (filter.getDireitoMaterial() != null) {
                c.add(Restrictions.eq("i.direitoMaterial", filter.getDireitoMaterial()));
            }

            if (filter.getId() != null) {
                c.add(Restrictions.eq("i.id", filter.getId()));
            }

            if (!filter.getSituacoes().isEmpty()) {
                c.add(Restrictions.in("i.situacao", filter.getSituacoes()));
            }

            if (StringUtils.isNotBlank(filter.getNomeEstudante())) {
                c.createAlias("i.estudante", "e");
                c.add(Restrictions.like("e.nome", filter.getNomeEstudante(), MatchMode.ANYWHERE));
            }

            if (filter.getDataSeminario() != null || StringUtils.isNotBlank(filter.getTituloSeminario())) {
                c.createAlias("i.seminario", "s");
                if (filter.getDataSeminario() != null) {
                    c.add(Restrictions.eq("s.data", filter.getDataSeminario()));
                }

                if (StringUtils.isNotBlank(filter.getTituloSeminario())) {
                    c.add(Restrictions.like("s.titulo", filter.getTituloSeminario(), MatchMode.ANYWHERE));
                }
            }

            Collection<Inscricao> inscricoes = c.list();
            inscricoes.forEach(this::inicializaLazyObjects);
            return inscricoes;

        } catch (Exception e) {
            throw new SeminariosCientificosException(e.getMessage());
        }
    }

    @SuppressWarnings("unchecked")
    @Cronometro
    public List<AlunoInscritoSeminarioDTO> getAlunosInscritosSeminario(String tituloSeminario) {
        Session session = this.getSession();
        try {

            /*
             * SELECT e.nome, ie.nome, i.direito_material, i.situacao, s.titulo FROM tb_inscricao i
             * JOIN tb_seminario s ON s.id = i.id_seminario
             * JOIN tb_estudante e ON i.id_estudante = e.id
             * JOIN tb_instituicao ie ON e.id_instituicao = ie.id
             *
             */

            String query = "SELECT new br.com.mauda.seminario.cientificos.dto.AlunoInscritoSeminarioDTO(e.nome, ie.nome, i.direitoMaterial, i.situacao) "
                + "FROM Inscricao i "
                + "JOIN i.seminario s "
                + "JOIN i.estudante e "
                + "JOIN e.instituicao ie "
                + "WHERE s.titulo LIKE :titulo";

            Query q = session.createQuery(query);
            q.setParameter("titulo", tituloSeminario);
            return q.list();
        } catch (Exception e) {
            throw new SeminariosCientificosException(e.getMessage());
        }
    }
}
