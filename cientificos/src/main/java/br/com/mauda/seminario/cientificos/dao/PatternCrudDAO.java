package br.com.mauda.seminario.cientificos.dao;

import java.io.Serializable;
import java.util.Collection;

import javax.inject.Inject;

import org.hibernate.Query;
import org.hibernate.Session;

import br.com.mauda.seminario.cientificos.dao.util.PossuiSession;
import br.com.mauda.seminario.cientificos.dto.FilterValidation;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.interceptor.Cronometro;
import br.com.mauda.seminario.cientificos.interceptor.OperacaoTransacional;
import br.com.mauda.seminario.cientificos.model.DataValidation;

public abstract class PatternCrudDAO<T extends DataValidation, F extends FilterValidation>
    implements Serializable, PossuiSession {

    private static final long serialVersionUID = 3723942253378506052L;
    private String findAllHQL;
    private String findByIdHQL;

    @Inject
    protected Session session;

    public PatternCrudDAO() {
    }

    public PatternCrudDAO(Class<T> entityClass) {
        this.findByIdHQL = "FROM " + entityClass.getName() + " as c WHERE c.id = :id";
        this.findAllHQL = "FROM " + entityClass.getName() + " as c ";
    }

    ////////////////////////////////////////////////////////////
    // METODOS AUXILIARES
    ////////////////////////////////////////////////////////////
    /**
     * Metodo auxiliar para inicializar um objeto que eh lazy
     *
     * @param object
     */
    public abstract void inicializaLazyObjects(T object);

    ////////////////////////////////////////////////////////////
    // METODOS DML DE RECUPERACAO DE INFORMACAO
    ////////////////////////////////////////////////////////////

    /**
     * Metodo que realiza uma busca pelo id na tabela da entidade T
     *
     * @param id
     * @return
     */
    @SuppressWarnings("unchecked")
    @Cronometro
    public T findById(Long id) {
        try {
            Query byIdQuery = this.session.createQuery(this.findByIdHQL);
            byIdQuery.setParameter("id", id);
            T object = (T) byIdQuery.uniqueResult();
            this.inicializaLazyObjects(object);
            return object;
        } catch (Exception e) {
            throw new SeminariosCientificosException(e);
        }
    }

    /**
     * Utilizado para buscas com o filtro da entidade, onde este contera as informacoes relacionadas com a filtragem de dados
     *
     * @param filter
     * @return
     */
    public abstract Collection<T> findByFilter(F filter);

    /**
     * Metodo que realiza a busca de todas as entidades da tabela da entidade T
     *
     * @return
     */
    @SuppressWarnings("unchecked")
    @Cronometro
    public Collection<T> findAll() {
        try {
            Query listQuery = this.session.createQuery(this.findAllHQL);
            Collection<T> collection = listQuery.list();
            for (T object : collection) {
                this.inicializaLazyObjects(object);
            }
            return collection;
        } catch (Exception e) {
            throw new SeminariosCientificosException(e);
        }
    }

    /////////////////////////////////////////
    // METODOS DML COM ALTERACAO NA BASE
    /////////////////////////////////////////

    /**
     * Metodo que realiza um insert na tabela da entidade T
     *
     * @param obj
     * @return
     */
    @OperacaoTransacional
    @Cronometro
    public void insert(T obj) {
        this.session.persist(obj);
    }

    /**
     * Metodo que realiza um update na tabela da entidade T
     *
     * @param obj
     * @return
     */
    @OperacaoTransacional
    @Cronometro
    public void update(T obj) {
        this.session.merge(obj);
    }

    /**
     * Metodo que realiza um delete na tabela da entidade T
     *
     * @param obj
     * @return
     */

    @OperacaoTransacional
    @Cronometro
    public void delete(T obj) {
        this.session.delete(obj);
    }

    @Override
    public Session getSession() {
        return this.session;
    }
}