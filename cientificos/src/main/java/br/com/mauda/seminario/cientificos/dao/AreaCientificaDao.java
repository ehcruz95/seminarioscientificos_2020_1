package br.com.mauda.seminario.cientificos.dao;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;

import br.com.mauda.seminario.cientificos.dto.AreaCientificaDTO;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.interceptor.Cronometro;
import br.com.mauda.seminario.cientificos.model.AreaCientifica;

@ApplicationScoped
public class AreaCientificaDao extends PatternCrudDAO<AreaCientifica, AreaCientificaDTO> implements HQLClause {

    public AreaCientificaDao() {
        super(AreaCientifica.class);
    }

    @Override
    public void inicializaLazyObjects(AreaCientifica object) {
        if (object != null && object.getCursos() != null) {
            object.getCursos().forEach(curso -> Hibernate.initialize(curso.getAreaCientifica()));
        }
    }

    @Override
    @Cronometro
    public Collection<AreaCientifica> findByFilter(AreaCientificaDTO filter) {

        Session session = this.getSession();

        try {
            StringBuilder sb = new StringBuilder("FROM AreaCientifica a");
            boolean whereFlag = false;
            Map<String, Object> parameters = new HashMap<>();

            if (filter.getId() != null) {
                sb.append(this.validateHqlClause(whereFlag)).append("a.id = :id");
                parameters.put("id", filter.getId());
            }
            if (!StringUtils.isBlank(filter.getNome())) {
                sb.append(this.validateHqlClause(whereFlag)).append("a.nome LIKE :nome");
                parameters.put("nome", "%" + filter.getNome() + "%");
            }

            Query q = session.createQuery(sb.toString());

            parameters.forEach(q::setParameter);

            Collection<AreaCientifica> areaCientificas = q.list();
            areaCientificas.forEach(this::inicializaLazyObjects);

            return areaCientificas;
        } catch (Exception e) {
            throw new SeminariosCientificosException(e);
        }
    }

}