package br.com.mauda.seminario.cientificos.bc;

import br.com.mauda.seminario.cientificos.dao.SeminarioDao;
import br.com.mauda.seminario.cientificos.dto.SeminarioDTO;
import br.com.mauda.seminario.cientificos.model.Seminario;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class SeminarioBC extends PatternCrudBC<Seminario, SeminarioDTO, SeminarioDao> {
}
