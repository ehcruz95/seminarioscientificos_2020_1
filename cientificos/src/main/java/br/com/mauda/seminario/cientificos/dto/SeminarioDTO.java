package br.com.mauda.seminario.cientificos.dto;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

public class SeminarioDTO implements Serializable, FilterValidation {

    /**
     *
     */
    private static final long serialVersionUID = -4409123402388941764L;
    private Long id;
    private Date data;
    private String descricao;
    private Boolean mesaRedonda;
    private String nomeAreaCientifica;
    private String nomeProfessor;
    private String titulo;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getData() {
        return this.data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean getMesaRedonda() {
        return this.mesaRedonda;
    }

    public void setMesaRedonda(Boolean mesaRedonda) {
        this.mesaRedonda = mesaRedonda;
    }

    public String getNomeAreaCientifica() {
        return this.nomeAreaCientifica;
    }

    public void setNomeAreaCientifica(String nomeAreaCientifica) {
        this.nomeAreaCientifica = nomeAreaCientifica;
    }

    public String getNomeProfessor() {
        return this.nomeProfessor;
    }

    public void setNomeProfessor(String nomeProfessor) {
        this.nomeProfessor = nomeProfessor;
    }

    public String getTitulo() {
        return this.titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    @Override
    public boolean validateForFindData() {
        return this.isSeminarioFieldsFilled() || this.isProfessorFieldsFilled() || this.isAreaCientificaFieldsFilled();
    }

    private boolean isSeminarioFieldsFilled() {
        return this.data != null || StringUtils.isNotBlank(this.descricao)
            || this.id != null || this.mesaRedonda != null || StringUtils.isNotBlank(this.titulo);
    }

    private boolean isProfessorFieldsFilled() {
        return StringUtils.isNotBlank(this.nomeProfessor);
    }

    private boolean isAreaCientificaFieldsFilled() {
        return StringUtils.isNotBlank(this.nomeAreaCientifica);
    }

}
