package br.com.mauda.seminario.cientificos.dto;

import br.com.mauda.seminario.cientificos.model.enums.SituacaoInscricaoEnum;

public class AlunoInscritoSeminarioDTO {

    private String nomeEstudante;
    private String nomeInstituicao;
    private Boolean direitoMaterial;
    private String situacaoInscricao;

    public AlunoInscritoSeminarioDTO(String nomeEstudante, String nomeInstituicao, Boolean direitoMaterial, SituacaoInscricaoEnum situacaoInscricao) {
        this.nomeEstudante = nomeEstudante;
        this.nomeInstituicao = nomeInstituicao;
        this.direitoMaterial = direitoMaterial;
        this.situacaoInscricao = situacaoInscricao.getNome();
    }

    public String getNomeEstudante() {
        return this.nomeEstudante;
    }

    public String getNomeInstituicao() {
        return this.nomeInstituicao;
    }

    public Boolean getDireitoMaterial() {
        return this.direitoMaterial;
    }

    public String getSituacaoInscricao() {
        return this.situacaoInscricao;
    }

}
