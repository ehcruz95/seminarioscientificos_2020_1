package br.com.mauda.seminario.cientificos.junit.tests.jasper;

import static br.com.mauda.seminario.cientificos.junit.util.AssertionsMauda.assertTrue;

import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.commons.lang3.time.StopWatch;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;

public class TesteJasper {

    private static final String JRXML = "reports/relatorioTeste.jrxml";
    private static final String JASPER = "reports/relatorioTeste.jasper";
    private static final String JRPRINT = "reports/relatorioTeste.jrprint";
    private static final String PDF = "reports/relatorioTeste.pdf";

    @DisplayName("Teste para verificar se esta gerando um relatorio")
    @Test
    public void teste() throws JRException {
        StopWatch cronometro = new StopWatch();
        cronometro.start();

        assertTrue(Files.exists(Paths.get(JRXML)), "Nao existe o arquivo " + JRXML);

        // Compilacao no formato jrxml para o jasper
        JasperCompileManager.compileReportToFile(JRXML);
        assertTrue(Files.exists(Paths.get(JASPER)), "Nao existe o arquivo " + JASPER);
        System.err.println("Tempo de compilacao jrxml -> jasper: " + cronometro.getTime());

        // Compilacao no formato jasper para o jrprint
        JasperFillManager.fillReportToFile(JASPER, null, new JREmptyDataSource(1));
        assertTrue(Files.exists(Paths.get(JRPRINT)), "Nao existe o arquivo " + JRPRINT);
        System.err.println("Tempo de compilacao jasper -> jrprint: " + cronometro.getTime());

        // Geracao do PDF
        JasperExportManager.exportReportToPdfFile(JRPRINT);
        assertTrue(Files.exists(Paths.get(PDF)), "Nao existe o arquivo " + PDF);
        System.err.println("Tempo de geracao do PDF: " + cronometro.getTime());

    }
}
