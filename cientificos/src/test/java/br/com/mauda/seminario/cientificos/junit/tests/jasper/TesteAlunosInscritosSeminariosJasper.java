package br.com.mauda.seminario.cientificos.junit.tests.jasper;

import static br.com.mauda.seminario.cientificos.junit.util.AssertionsMauda.assertFalse;
import static br.com.mauda.seminario.cientificos.junit.util.AssertionsMauda.assertTrue;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import br.com.mauda.seminario.cientificos.bc.InscricaoBC;
import br.com.mauda.seminario.cientificos.dto.AlunoInscritoSeminarioDTO;
import br.com.mauda.seminario.cientificos.junit.massa.MassaSeminario;
import br.com.mauda.seminario.cientificos.util.WeldContext;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class TesteAlunosInscritosSeminariosJasper {

    private static final String JRXML = "reports/alunosInscritosSeminario.jrxml";
    private static final String JASPER = "reports/AlunosInscritosSeminario.jasper";
    private static final String JRPRINT = "reports/AlunosInscritosSeminario.jrprint";

    protected InscricaoBC bc = WeldContext.getInstanciatedClass(InscricaoBC.class);

    @BeforeAll
    public static void compilaReport() throws JRException {
        assertTrue(Files.exists(Paths.get(JRXML)), "Nao existe o arquivo " + JRXML);

        // Compilacao no formato jrxml para o jasper
        JasperCompileManager.compileReportToFile(JRXML);
    }

    @Tag("jasperTest")
    @DisplayName("Relatorio de Alunos inscritos por seminario")
    @ParameterizedTest(name = "Relatorio de alunos inscritos do Seminario [{arguments}]")
    @EnumSource(MassaSeminario.class)
    public void relatorioPorSeminario(MassaSeminario seminarioEnum) throws ParseException, JRException {
        // Obtem as informacoes do banco de dados
        List<AlunoInscritoSeminarioDTO> alunos = this.bc.getAlunosInscritosSeminario(seminarioEnum.getTitulo());

        Map<String, Object> parameters = new HashMap<>();
        // Criar um parametro no Jaspersoft de nome tituloSeminario
        parameters.put("tituloSeminario", seminarioEnum.getTitulo());
        parameters.put("dataAtual", new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date()));

        assertTrue(Files.exists(Paths.get(JASPER)), "Nao existe o arquivo " + JASPER);

        // Compilacao no formato jasper para o jrprint
        JasperFillManager.fillReportToFile(JASPER, parameters, new JRBeanCollectionDataSource(alunos, false));

        assertTrue(Files.exists(Paths.get(JRPRINT)), "Nao existe o arquivo " + JRPRINT);

        // Geracao do PDF
        String nomeArquivoPDF = "reports/" + seminarioEnum.getTitulo() + ".pdf";
        JasperExportManager.exportReportToPdfFile(JRPRINT, nomeArquivoPDF);

        assertTrue(Files.exists(Paths.get(nomeArquivoPDF)), "Nao existe o arquivo " + nomeArquivoPDF);
    }
}