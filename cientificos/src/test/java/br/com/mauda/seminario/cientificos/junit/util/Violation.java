package br.com.mauda.seminario.cientificos.junit.util;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

public class Violation {

    ConstraintViolation<?> violation;

    public Violation(ConstraintViolationException exception) {
        this.violation = exception.getConstraintViolations().iterator().next();
    }

    public String getFieldName() {
        return this.violation.getPropertyPath().toString();
    }

    public String getMessage() {
        return this.violation.getMessageTemplate();
    }
}
