package br.com.mauda.seminario.cientificos.junit.contract.validation;

import static br.com.mauda.seminario.cientificos.junit.util.AssertionsMauda.assertEquals;

import java.util.Date;

import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.time.DateUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import br.com.mauda.seminario.cientificos.junit.util.Violation;

public interface TestsBVDateFutureField extends TestsBVGenericField<Date> {

    @Test
    @DisplayName("Campo preechido com data antes da atual")
    public default void validarValorAnteriorDataAtual() {
        this.setValue(DateUtils.addDays(new Date(), -30));
        ConstraintViolationException exception = Assertions.assertThrows(ConstraintViolationException.class, () -> this.executionMethod());
        Violation violation = new Violation(exception);
        assertEquals(this.getFieldName(), violation.getFieldName(), "O campo '" + this.getFieldName() + "' nao eh o que esta sendo validado");
        assertEquals("{javax.validation.constraints.Future.message}", violation.getMessage(),
            "O campo '" + this.getFieldName() + "' nao pode estar antes de hoje");
    }
}