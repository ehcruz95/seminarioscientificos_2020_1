package br.com.mauda.seminario.cientificos.junit.contract.validation;

import static br.com.mauda.seminario.cientificos.junit.util.AssertionsMauda.assertEquals;

import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import br.com.mauda.seminario.cientificos.junit.util.Violation;

public interface TestsBVStringField extends TestsBVGenericField<String> {

    default int getMaxSizeField() {
        return 50;
    }

    @Override
    @Test
    @DisplayName("Campo com valor nulo")
    public default void validarNulo() {
        this.setValue(null);
        ConstraintViolationException exception = Assertions.assertThrows(ConstraintViolationException.class, () -> this.executionMethod());
        Violation violation = new Violation(exception);
        assertEquals(this.getFieldName(), violation.getFieldName(), "O campo '" + this.getFieldName() + "' nao eh o que esta sendo validado");
        assertEquals("{org.hibernate.validator.constraints.NotBlank.message}", violation.getMessage(),
            "O campo '" + this.getFieldName() + "' nao pode ser vazio");
    }

    @Test
    @DisplayName("Campo preechido com somente espacos em branco")
    public default void validarValorComEspacos() {
        this.setValue("     ");
        ConstraintViolationException exception = Assertions.assertThrows(ConstraintViolationException.class, () -> this.executionMethod());
        Violation violation = new Violation(exception);
        assertEquals(this.getFieldName(), violation.getFieldName(), "O campo '" + this.getFieldName() + "' nao eh o que esta sendo validado");
        assertEquals("{org.hibernate.validator.constraints.NotBlank.message}", violation.getMessage(),
            "O campo '" + this.getFieldName() + "' nao pode ser vazio");
    }

    @Test
    @DisplayName("Campo com numero de caracteres maior que o permitido")
    public default void validarValorcomTamanhoExcedido() {
        this.setValue(RandomStringUtils.random(this.getMaxSizeField() + 1));
        ConstraintViolationException exception = Assertions.assertThrows(ConstraintViolationException.class, () -> this.executionMethod());
        Violation violation = new Violation(exception);
        assertEquals(this.getFieldName(), violation.getFieldName(), "O campo '" + this.getFieldName() + "' nao eh o que esta sendo validado");
        assertEquals("{javax.validation.constraints.Size.message}", violation.getMessage(),
            "O campo '" + this.getFieldName() + "' nao pode ultrapassar " + this.getMaxSizeField() + " caracteres");
    }
}