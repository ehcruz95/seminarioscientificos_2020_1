package br.com.mauda.seminario.cientificos.junit.contract.validation;

import static br.com.mauda.seminario.cientificos.junit.util.AssertionsMauda.assertEquals;

import javax.validation.ConstraintViolationException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import br.com.mauda.seminario.cientificos.junit.util.Violation;

public interface TestsBVGenericField<E extends Object> {

    void setValue(E value);

    void executionMethod();

    String getFieldName();

    @Test
    @DisplayName("Campo com valor nulo")
    public default void validarNulo() {
        this.setValue(null);
        ConstraintViolationException exception = Assertions.assertThrows(ConstraintViolationException.class, () -> this.executionMethod());
        Violation violation = new Violation(exception);
        assertEquals(this.getFieldName(), violation.getFieldName(), "O campo '" + this.getFieldName() + "' nao eh o que esta sendo validado");
        assertEquals("{javax.validation.constraints.NotNull.message}", violation.getMessage(),
            "O campo '" + this.getFieldName() + "' nao pode ser vazio");
    }
}