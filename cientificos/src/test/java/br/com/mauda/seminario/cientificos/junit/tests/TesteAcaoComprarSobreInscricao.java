package br.com.mauda.seminario.cientificos.junit.tests;

import static br.com.mauda.seminario.cientificos.junit.util.AssertionsMauda.assertEquals;
import static br.com.mauda.seminario.cientificos.junit.util.AssertionsMauda.assertThrows;

import java.util.Date;

import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.provider.EnumSource;

import br.com.mauda.seminario.cientificos.bc.InscricaoBC;
import br.com.mauda.seminario.cientificos.junit.converter.dao.AcaoInscricaoDTODAOConverter;
import br.com.mauda.seminario.cientificos.junit.converter.dto.AcaoInscricaoDTOConverter;
import br.com.mauda.seminario.cientificos.junit.dto.AcaoInscricaoDTO;
import br.com.mauda.seminario.cientificos.junit.executable.EstudanteExecutable;
import br.com.mauda.seminario.cientificos.junit.executable.InscricaoExecutable;
import br.com.mauda.seminario.cientificos.junit.executable.SeminarioExecutable;
import br.com.mauda.seminario.cientificos.junit.massa.MassaInscricaoComprar;
import br.com.mauda.seminario.cientificos.junit.util.Violation;
import br.com.mauda.seminario.cientificos.model.Inscricao;
import br.com.mauda.seminario.cientificos.model.enums.SituacaoInscricaoEnum;
import br.com.mauda.seminario.cientificos.util.EnumUtils;
import br.com.mauda.seminario.cientificos.util.WeldContext;

public class TesteAcaoComprarSobreInscricao {

    protected InscricaoBC bc = WeldContext.getInstanciatedClass(InscricaoBC.class);
    protected AcaoInscricaoDTOConverter converter = new AcaoInscricaoDTOConverter();
    protected AcaoInscricaoDTO acaoInscricaoDTO;

    @BeforeEach
    void beforeEach() {
        this.acaoInscricaoDTO = this.converter.create(EnumUtils.getInstanceRandomly(MassaInscricaoComprar.class));
    }

    @Tag("beanValidationTest")
    @DisplayName("Compra de uma inscricao para o Seminario")
    @ParameterizedTest(name = "Compra da inscricao [{arguments}] para o Seminario")
    @EnumSource(MassaInscricaoComprar.class)
    public void comprarInscricao(@ConvertWith(AcaoInscricaoDTODAOConverter.class) AcaoInscricaoDTO object) {
        // Verifica se os atributos estao preenchidos
        Assertions.assertAll(new SeminarioExecutable(object.getSeminario()));

        // Verifica se os atributos estao preenchidos
        Assertions.assertAll(new EstudanteExecutable(object.getEstudante()));

        Inscricao inscricao = object.getInscricao();

        // Significa que as inscricoes nao foram geradas automaticamente pelo construtor do seminario
        Assertions.assertNotNull(inscricao);

        // Verifica se os atributos estao preenchidos
        Assertions.assertAll(new InscricaoExecutable(inscricao));

        // Compra a inscricao pro seminario
        this.bc.comprar(inscricao, object.getEstudante(), object.getDireitoMaterial());

        // Verifica se a situacao da inscricao ficou como comprado
        assertEquals(inscricao.getSituacao(), SituacaoInscricaoEnum.COMPRADO,
            "Situacao da inscricao nao eh comprado - trocar a situacao no metodo realizarCheckIn()");

        // Verifica se os atributos estao preenchidos
        Assertions.assertAll(new InscricaoExecutable(inscricao));
    }

    @Tag("beanValidationTest")
    @Test
    @DisplayName("Compra com inscricao nula")
    public void validarCompraComInscricaoNula() {
        ConstraintViolationException exception = Assertions.assertThrows(ConstraintViolationException.class,
            () -> this.bc.comprar(null, this.acaoInscricaoDTO.getEstudante(), this.acaoInscricaoDTO.getDireitoMaterial()));
        Violation violation = new Violation(exception);
        assertEquals("comprar.arg0", violation.getFieldName(), "O parametro 'inscricao' nao eh o que esta sendo validado");
        assertEquals("{javax.validation.constraints.NotNull.message}", violation.getMessage(), "O campo 'inscricao' nao pode ser nulo");
    }

    @Tag("beanValidationTest")
    @Test
    @DisplayName("Compra com estudante nulo")
    public void validarCompraComEstudanteNulo() {
        ConstraintViolationException exception = Assertions.assertThrows(ConstraintViolationException.class,
            () -> this.bc.comprar(this.acaoInscricaoDTO.getInscricao(), null, this.acaoInscricaoDTO.getDireitoMaterial()));
        Violation violation = new Violation(exception);
        assertEquals("comprar.arg1", violation.getFieldName(), "O parametro 'estudante' nao eh o que esta sendo validado");
        assertEquals("{javax.validation.constraints.NotNull.message}", violation.getMessage(), "O campo 'estudante' nao pode ser nulo");
    }

    @Tag("beanValidationTest")
    @Test
    @DisplayName("Compra com direito material nulo")
    public void validarCompraComDireitoMaterialNulo() {
        ConstraintViolationException exception = Assertions.assertThrows(ConstraintViolationException.class,
            () -> this.bc.comprar(this.acaoInscricaoDTO.getInscricao(), this.acaoInscricaoDTO.getEstudante(), null));
        Violation violation = new Violation(exception);
        assertEquals("comprar.arg2", violation.getFieldName(), "O parametro 'Direito Material' nao eh o que esta sendo validado");
        assertEquals("{javax.validation.constraints.NotNull.message}", violation.getMessage(),
            "O campo 'Direito Material' nao pode ser nulo");
    }

    @Tag("beanValidationTest")
    @Test
    @DisplayName("Compra com situacao da inscricao diferente de Disponivel")
    public void validarCompraComSituacaoInscricaoNaoDisponivel() throws IllegalAccessException {
        Inscricao inscricao = this.acaoInscricaoDTO.getInscricao();

        // Metodo que seta a situacao da inscricao como COMPRADO usando reflections
        FieldUtils.writeDeclaredField(inscricao, "situacao", SituacaoInscricaoEnum.COMPRADO, true);
        assertThrows(() -> this.bc.comprar(inscricao, this.acaoInscricaoDTO.getEstudante(),
            this.acaoInscricaoDTO.getDireitoMaterial()), "ER0042");

        // Metodo que seta a situacao da inscricao como CHECKIN usando reflections
        FieldUtils.writeDeclaredField(inscricao, "situacao", SituacaoInscricaoEnum.CHECKIN, true);
        assertThrows(() -> this.bc.comprar(inscricao, this.acaoInscricaoDTO.getEstudante(),
            this.acaoInscricaoDTO.getDireitoMaterial()), "ER0042");
    }

    @Tag("MapeamentoDAOTest")
    @Test
    @DisplayName("Compra de uma inscricao após a data do Seminario")
    public void validarCompraAposDataSeminario() {
        Inscricao inscricao = this.acaoInscricaoDTO.getInscricao();

        // Diminui a data do seminario em 30 dias
        this.acaoInscricaoDTO.getSeminario().setData(DateUtils.addDays(new Date(), -30));

        ConstraintViolationException exception = Assertions.assertThrows(ConstraintViolationException.class,
            () -> this.bc.comprar(inscricao, this.acaoInscricaoDTO.getEstudante(), this.acaoInscricaoDTO.getDireitoMaterial()));
        Violation violation = new Violation(exception);
        assertEquals("comprar.arg0.seminario.data", violation.getFieldName(),
            "O parametro 'data' da classe Seminario nao eh o que esta sendo validado");
        assertEquals("{javax.validation.constraints.Future.message}", violation.getMessage(),
            "O campo 'data' do seminario deve estar no futuro");
    }
}