package br.com.mauda.seminario.cientificos.junit.contract.validation;

import static br.com.mauda.seminario.cientificos.junit.util.AssertionsMauda.assertEquals;

import javax.validation.ConstraintViolationException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import br.com.mauda.seminario.cientificos.junit.util.Violation;

public interface TestsBVEmailField extends TestsBVGenericField<String> {

    default int getMaxSizeField() {
        return 50;
    }

    @Override
    @Test
    @DisplayName("Email com valor nulo")
    public default void validarNulo() {
        this.setValue(null);
        ConstraintViolationException exception = Assertions.assertThrows(ConstraintViolationException.class, () -> this.executionMethod());
        Violation violation = new Violation(exception);
        assertEquals(this.getFieldName(), violation.getFieldName(), "O campo '" + this.getFieldName() + "' nao eh o que esta sendo validado");
        assertEquals("{org.hibernate.validator.constraints.NotBlank.message}", violation.getMessage(),
            "O campo '" + this.getFieldName() + "' nao pode ser vazio");
    }

    @Test
    @DisplayName("Campo com numero de caracteres maior que o permitido")
    public default void validarNomeExcedido() {
        this.setValue("eu101010101010101010101101010101@instituicao.com.br");
        ConstraintViolationException exception = Assertions.assertThrows(ConstraintViolationException.class, () -> this.executionMethod());
        Violation violation = new Violation(exception);
        assertEquals(this.getFieldName(), violation.getFieldName(), "O campo '" + this.getFieldName() + "' nao eh o que esta sendo validado");
        assertEquals("{javax.validation.constraints.Size.message}", violation.getMessage(),
            "O campo '" + this.getFieldName() + "' nao pode ultrapassar " + this.getMaxSizeField() + " caracteres");
    }

    @Test
    @DisplayName("Email sem arroba")
    public default void validarEmailSemArroba() {
        this.setValue("estudanteinstituicao.com.br");
        ConstraintViolationException exception = Assertions.assertThrows(ConstraintViolationException.class, () -> this.executionMethod());
        Violation violation = new Violation(exception);
        assertEquals(this.getFieldName(), violation.getFieldName(), "O campo '" + this.getFieldName() + "' nao eh o que esta sendo validado");
        assertEquals("{org.hibernate.validator.constraints.Email.message}", violation.getMessage(),
            "O email nao possui um formato valido");
    }

    @Test
    @DisplayName("Email sem ponto")
    public default void validarEmailSemPonto() {
        this.setValue("estudante@instituicaocombr");
        ConstraintViolationException exception = Assertions.assertThrows(ConstraintViolationException.class, () -> this.executionMethod());
        Violation violation = new Violation(exception);
        assertEquals(this.getFieldName(), violation.getFieldName(), "O campo '" + this.getFieldName() + "' nao eh o que esta sendo validado");
        assertEquals("{org.hibernate.validator.constraints.Email.message}", violation.getMessage(),
            "O email nao possui um formato valido");
    }

}
