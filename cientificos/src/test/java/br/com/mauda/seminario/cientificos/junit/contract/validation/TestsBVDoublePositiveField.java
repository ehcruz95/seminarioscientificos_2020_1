package br.com.mauda.seminario.cientificos.junit.contract.validation;

import static br.com.mauda.seminario.cientificos.junit.util.AssertionsMauda.assertEquals;

import javax.validation.ConstraintViolationException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import br.com.mauda.seminario.cientificos.junit.util.Violation;

public interface TestsBVDoublePositiveField extends TestsBVGenericField<Double> {

    @Test
    @DisplayName("Campo preechido com valor negativo")
    public default void validarValorNegativo() {
        this.setValue(-1d);
        ConstraintViolationException exception = Assertions.assertThrows(ConstraintViolationException.class, () -> this.executionMethod());
        Violation violation = new Violation(exception);
        assertEquals(this.getFieldName(), violation.getFieldName(), "O campo '" + this.getFieldName() + "' nao eh o que esta sendo validado");
        assertEquals("{javax.validation.constraints.DecimalMin.message}", violation.getMessage(),
            "O campo '" + this.getFieldName() + "' nao pode ser negativo");
    }

    @Test
    @DisplayName("Campo preechido com valor zero")
    public default void validarValorZero() {
        this.setValue(0d);
        ConstraintViolationException exception = Assertions.assertThrows(ConstraintViolationException.class, () -> this.executionMethod());
        Violation violation = new Violation(exception);
        assertEquals(this.getFieldName(), violation.getFieldName(), "O campo '" + this.getFieldName() + "' nao eh o que esta sendo validado");
        assertEquals("{javax.validation.constraints.DecimalMin.message}", violation.getMessage(),
            "O campo '" + this.getFieldName() + "' nao pode ser zero");
    }
}