package br.com.mauda.seminario.cientificos.junit.tests;

import static br.com.mauda.seminario.cientificos.junit.util.AssertionsMauda.assertAll;
import static br.com.mauda.seminario.cientificos.junit.util.AssertionsMauda.assertEquals;
import static br.com.mauda.seminario.cientificos.junit.util.AssertionsMauda.assertNull;
import static br.com.mauda.seminario.cientificos.junit.util.AssertionsMauda.assertTrue;

import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.provider.EnumSource;

import br.com.mauda.seminario.cientificos.bc.EstudanteBC;
import br.com.mauda.seminario.cientificos.junit.contract.validation.TestsBVEmailField;
import br.com.mauda.seminario.cientificos.junit.contract.validation.TestsBVStringField;
import br.com.mauda.seminario.cientificos.junit.converter.EstudanteConverter;
import br.com.mauda.seminario.cientificos.junit.converter.dao.EstudanteDAOConverter;
import br.com.mauda.seminario.cientificos.junit.executable.EstudanteExecutable;
import br.com.mauda.seminario.cientificos.junit.massa.MassaEstudante;
import br.com.mauda.seminario.cientificos.junit.util.Violation;
import br.com.mauda.seminario.cientificos.model.Estudante;
import br.com.mauda.seminario.cientificos.util.EnumUtils;
import br.com.mauda.seminario.cientificos.util.WeldContext;

public class TesteEstudante {

    protected EstudanteBC bc = WeldContext.getInstanciatedClass(EstudanteBC.class);
    protected EstudanteConverter converter = new EstudanteConverter();
    protected Estudante estudante;

    @BeforeEach
    void beforeEach() {
        this.estudante = this.converter.create(EnumUtils.getInstanceRandomly(MassaEstudante.class));
    }

    @Tag("beanValidationTest")
    @DisplayName("Criacao de um Estudante")
    @ParameterizedTest(name = "Criacao do Estudante [{arguments}]")
    @EnumSource(MassaEstudante.class)
    public void criar(@ConvertWith(EstudanteDAOConverter.class) Estudante object) {
        // Verifica se os atributos estao preenchidos corretamente
        assertAll(new EstudanteExecutable(object));

        // Realiza o insert no banco de dados atraves da Business Controller
        this.bc.insert(object);

        // Verifica se o id eh maior que zero, indicando que foi inserido no banco
        assertTrue(object.getId() > 0, "Insert nao foi realizado corretamente pois o ID do objeto nao foi gerado");

        // Obtem uma nova instancia do BD a partir do ID gerado
        Estudante objectBD = this.bc.findById(object.getId());

        // Realiza as verificacoes entre o objeto em memoria e o obtido do banco
        assertAll(new EstudanteExecutable(object, objectBD));
    }

    @Tag("beanValidationTest")
    @DisplayName("Atualizacao dos atributos de um Estudante")
    @ParameterizedTest(name = "Atualizacao do Estudante [{arguments}]")
    @EnumSource(MassaEstudante.class)
    public void atualizar(@ConvertWith(EstudanteDAOConverter.class) Estudante object) {
        // Cria o objeto
        this.criar(object);

        // Atualiza as informacoes de um objeto
        this.converter.update(object, EnumUtils.getInstanceRandomly(MassaEstudante.class));

        // Realiza o update no banco de dados atraves da Business Controller
        this.bc.update(object);

        // Obtem uma nova instancia do BD a partir do ID gerado
        Estudante objectBD = this.bc.findById(object.getId());

        // Realiza as verificacoes entre o objeto em memoria e o obtido do banco
        assertAll(new EstudanteExecutable(object, objectBD));

        // Realiza o delete no banco de dados atraves da Business Controller para nao deixar o registro
        this.bc.delete(object);
    }

    @Tag("beanValidationTest")
    @DisplayName("Delecao de um Estudante")
    @ParameterizedTest(name = "Delecao do Estudante [{arguments}]")
    @EnumSource(MassaEstudante.class)
    public void deletar(@ConvertWith(EstudanteDAOConverter.class) Estudante object) {
        // Realiza a insercao do objeto no banco de dados
        this.criar(object);

        // Remove o objeto do BD
        this.bc.delete(object);

        // Obtem o objeto do BD a partir do ID do objeto
        Estudante objectBD = this.bc.findById(object.getId());

        // Verifica se o objeto deixou de existir no BD
        assertNull(objectBD, "O objeto deveria estar deletado do banco de dados");
    }

    @Tag("beanValidationTest")
    @Test
    @DisplayName("Criacao de um estudante nulo")
    public void validarNulo() {
        ConstraintViolationException exception = Assertions.assertThrows(ConstraintViolationException.class,
            () -> this.bc.insert(null));
        Violation violation = new Violation(exception);
        assertEquals("insert.arg0", violation.getFieldName(), "O campo estudante nao eh o que esta sendo validado");
        assertEquals("{javax.validation.constraints.NotNull.message}", violation.getMessage(),
            "O campo estudante nao pode ser vazio");
    }

    @Tag("beanValidationTest")
    @Nested
    @DisplayName("Testes para o email do Estudante")
    class EmailEstudante implements TestsBVEmailField {

        @Override
        public void setValue(String value) {
            TesteEstudante.this.estudante.setEmail(value);
        }

        @Override
        public void executionMethod() {
            TesteEstudante.this.bc.insert(TesteEstudante.this.estudante);
        }

        @Override
        public String getFieldName() {
            return "email";
        }
    }

    @Tag("beanValidationTest")
    @Nested
    @DisplayName("Testes para o nome do Estudante")
    class NomeEstudante implements TestsBVStringField {

        @Override
        public void setValue(String value) {
            TesteEstudante.this.estudante.setNome(value);
        }

        @Override
        public void executionMethod() {
            TesteEstudante.this.bc.insert(TesteEstudante.this.estudante);
        }

        @Override
        public String getFieldName() {
            return "nome";
        }
    }

    @Tag("beanValidationTest")
    @Nested
    @DisplayName("Testes para o telefone do Estudante")
    class TelefoneEstudante implements TestsBVStringField {

        @Override
        public void setValue(String value) {
            TesteEstudante.this.estudante.setTelefone(value);
        }

        @Override
        public void executionMethod() {
            TesteEstudante.this.bc.insert(TesteEstudante.this.estudante);
        }

        @Override
        public String getFieldName() {
            return "telefone";
        }

        @Override
        public int getMaxSizeField() {
            return 15;
        }
    }

    @Tag("beanValidationTest")
    @Nested
    @DisplayName("Testes para a Instituicao dentro do Estudante")
    class InstituicaoDoEstudante {

        @Tag("beanValidationTest")
        @Test
        @DisplayName("Criacao de um estudante com Instituicao nula")
        public void validarNulo() throws IllegalAccessException {
            // Metodo que seta a instituicao como null usando reflections
            FieldUtils.writeDeclaredField(TesteEstudante.this.estudante, "instituicao", null, true);
            ConstraintViolationException exception = Assertions.assertThrows(ConstraintViolationException.class,
                () -> TesteEstudante.this.bc.insert(TesteEstudante.this.estudante));
            Violation violation = new Violation(exception);
            assertEquals("instituicao", violation.getFieldName(), "O campo instituicao nao eh o que esta sendo validado");
            assertEquals("{javax.validation.constraints.NotNull.message}", violation.getMessage(),
                "O campo instituicao nao pode ser vazio");
        }

        @Tag("beanValidationTest")
        @Nested
        @DisplayName("Testes para a cidade da Instituicao")
        class CidadeInstituicao implements TestsBVStringField {

            @Override
            public void setValue(String value) {
                TesteEstudante.this.estudante.getInstituicao().setCidade(value);
            }

            @Override
            public void executionMethod() {
                TesteEstudante.this.bc.insert(TesteEstudante.this.estudante);
            }

            @Override
            public String getFieldName() {
                return "instituicao.cidade";
            }
        }

        @Tag("beanValidationTest")
        @Nested
        @DisplayName("Testes para o estado da Instituicao")
        class EstadoInstituicao implements TestsBVStringField {

            @Override
            public void setValue(String value) {
                TesteEstudante.this.estudante.getInstituicao().setEstado(value);
            }

            @Override
            public void executionMethod() {
                TesteEstudante.this.bc.insert(TesteEstudante.this.estudante);
            }

            @Override
            public String getFieldName() {
                return "instituicao.estado";
            }
        }

        @Tag("beanValidationTest")
        @Nested
        @DisplayName("Testes para o nome da Instituicao")
        class NomeInstituicao implements TestsBVStringField {

            @Override
            public void setValue(String value) {
                TesteEstudante.this.estudante.getInstituicao().setNome(value);
            }

            @Override
            public void executionMethod() {
                TesteEstudante.this.bc.insert(TesteEstudante.this.estudante);
            }

            @Override
            public String getFieldName() {
                return "instituicao.nome";
            }

            @Override
            public int getMaxSizeField() {
                return 100;
            }
        }

        @Tag("beanValidationTest")
        @Nested
        @DisplayName("Testes para o pais da Instituicao")
        class PaisInstituicao implements TestsBVStringField {

            @Override
            public void setValue(String value) {
                TesteEstudante.this.estudante.getInstituicao().setPais(value);
            }

            @Override
            public void executionMethod() {
                TesteEstudante.this.bc.insert(TesteEstudante.this.estudante);
            }

            @Override
            public String getFieldName() {
                return "instituicao.pais";
            }
        }

        @Tag("beanValidationTest")
        @Nested
        @DisplayName("Testes para a sigla da Instituicao")
        class SiglaInstituicao implements TestsBVStringField {

            @Override
            public void setValue(String value) {
                TesteEstudante.this.estudante.getInstituicao().setSigla(value);
            }

            @Override
            public void executionMethod() {
                TesteEstudante.this.bc.insert(TesteEstudante.this.estudante);
            }

            @Override
            public String getFieldName() {
                return "instituicao.sigla";
            }

            @Override
            public int getMaxSizeField() {
                return 10;
            }
        }
    }
}